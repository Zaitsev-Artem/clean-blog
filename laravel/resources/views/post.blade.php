@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#create" style="margin-top: -1%;">Create post</button></div>

                <div class="panel-body">
                   
                        



                    <div class="row">
                        <form method="post" action="/post/edit/{{$post->id}}" enctype="multipart/form-data" >
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="title">Title:</label>
                                <input type="text" name="title" class="form-control" value="{{$post->title}}" id="title" required>
                            </div>
                            <div class="form-group">
                                <label for="short">Short title:</label>
                                <input type="text" name="short" class="form-control" value="{{$post->short_title}}" id="short" required>
                            </div>
                            <div class="form-group">
                                <label for="content">Content:</label>
                                <textarea rows="6" name="content" id="content" class="form-control" required minlength="50">
                                    {{$post->content}}
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="cover">Image:</label>
                                <input type="file" name="cover" accept="image/*" class="form-control" id="cover">
                                <img src="{{asset('/images/public/'.$post->cover)}}" width="300px">
                            </div>



                        <button type="submit" id="create_btn" class="btn btn-success" >Save</button>
                        <a href="/post/delete/{{$post->id}}" id="delete" class="btn btn-warning">Delete</a>
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div id="create" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Create post</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/post/store" enctype="multipart/form-data" >
                     {{csrf_field()}}
                <div class="form-group">
                    <label for="title">Title:</label>
                    <input type="text" name="title" class="form-control" id="title" required>
                </div>
                <div class="form-group">
                    <label for="short">Short title:</label>
                    <input type="text" name="short" class="form-control" id="short" required>
                </div>
                <div class="form-group">
                    <label for="content">Content:</label>
                    <textarea rows="6" name="content" id="content" class="form-control" required minlength="50"></textarea>
                </div>
                <div class="form-group">
                    <label for="cover">Content:</label>
                    <input type="file" name="cover" accept="image/*" class="form-control" id="cover" required>
                </div>

            </div>
            <div class="modal-footer">
                <button type="submit" id="create_btn" class="btn btn-success" >Post it</button>
                </form>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

@endsection
