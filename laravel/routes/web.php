<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::get('/', 'HomeController@index');
Route::get('/post/{id}', 'HomeController@post');

Route::get('/home','AdminController@index');
Route::post('/post/store','AdminController@create');
Route::get('/post/delete/{id}','AdminController@delete');
Route::post('/post/edit/{id}','AdminController@edit_store');
Route::get('/post/edit/{id}','AdminController@edit');