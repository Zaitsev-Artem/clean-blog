<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
    }


    public function index()
    {
	    $posts= Post::all();
        return view('blog.index',compact('posts'));
    }
	public function post($id)
	{
		$post = Post::find($id);
		return view('blog.post',compact('post'));
	}

}
