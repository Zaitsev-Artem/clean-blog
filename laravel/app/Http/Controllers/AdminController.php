<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;
class AdminController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(){

		$posts = Post::paginate(10);

		return view('home',compact('posts'));
	}
	public function edit($id){

		$post = Post::find($id);

		return view('post',compact('post'));
	}

	public function create(Request $request){


		$user = Auth::user();

			$post = new Post();
			$post->title = $request->get('title');
			$post->short_title = $request->get('short');
			$post->content = $request->get('content');
			$imageName = time().'.'.$request->file('cover')
					->getClientOriginalExtension();
			$request->file('cover')->move(public_path('/images/public'), $imageName);
			$post->cover = $imageName;
			$post->author = $user->name;
			$post->save();

			return redirect('/home');


	}

	public function edit_store(Request $request,$id){
		$user = Auth::user();

		$post = Post::find($id);
		$post->title = $request->get('title');
		$post->short_title = $request->get('short');
		$post->content = $request->get('content');
		if($request->file('cover')){
			$imageName = time().'.'.$request->file('cover')
					->getClientOriginalExtension();
			$request->file('cover')->move(public_path('/images/public'), $imageName);
			$post->cover = $imageName;
		}
		$post->author = $user->name;
		$post->save();

		return redirect('/home');

	}

	public function delete($id){

		 Post::find($id)->delete();
		return redirect('/home');

	}
}
